package thisSuper;

class Animal {
	String color = "WHITE..PARENT CLASS"; // parent class instance variable
}

public class color extends Animal {
	
	void printcolor () {
		String color = "This is BLACK color";
		System.out.println("color"); // Local class
		System.out.println("this.color"); // current class
		System.out.println("super.color"); // parent instance variable
	}

	public static void main(String[] args) {
		color c = new color();
		c.printcolor();
	}

}
