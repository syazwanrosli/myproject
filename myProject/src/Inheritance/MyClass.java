package Inheritance;

public class MyClass extends GrandChild {

	public static void main(String[] args) {
			
			MyClass m = new MyClass();
			m.method1();
			m.method2();
			m.mymethod();
			m.gcmethod();
	}

}
