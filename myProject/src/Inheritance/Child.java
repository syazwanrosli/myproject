package Inheritance;

public class Child extends Parent {  // inherited parent class 

	public void mymethod() {
		System.out.println("This is CHILD CLASS method...");
	}
	
	public static void main(String [] args) {
		
		Child c = new Child();  // object created
		c.method1(); // calling parent method
		c.method2(); // calling second method
	}

}
