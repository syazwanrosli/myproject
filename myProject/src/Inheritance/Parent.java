package Inheritance;

public class Parent {

	public void method1 () {
		System.out.println("This is PARENT method logic...");
	}
	
	public void method2 () {
		System.out.println("Method 2 output ... Parent method");
	}
}
