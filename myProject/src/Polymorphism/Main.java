package Polymorphism;

public class Main {

	public static void main(String[] args) {
		
		Animal a = new Animal();	// creating object
		Dog d = new Dog();			//		"
		Cat c = new Cat();			//		"
		
		d.sound(); // calling child method
		c.sound(); // calling another child method
		a.sound(); // calling parent method

	}

}
