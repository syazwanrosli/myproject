package Polymorphism;

public class Dog extends Animal { // dog is child class... and animal is parent class
	
	public void sound() {
		System.out.println("This is DOG... This is his own logic...");
	}

}
