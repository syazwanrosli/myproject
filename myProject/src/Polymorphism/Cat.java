package Polymorphism;


public class Cat extends Animal { // cat is child class .. animal is parent class
	
	public void sound() {
		System.out.println("Iam a CAT... its my own logic..");
	}

}
