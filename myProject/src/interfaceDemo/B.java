package interfaceDemo;

public class B implements A {
	
	@Override
	public void method1() {
		System.out.println("This is a interface method1...");
	}

	@Override
	public void method2() {
		System.out.println("Method2.. This is a interface method2..");
	}
	
	void thanks() {
		System.out.println("This is my own method, Thanks interface A");
	}
	
	
	public static void main(String[] args) {
		B obj = new B();
		obj.method1();
		obj.method2();
		obj.thanks();
	}

}
