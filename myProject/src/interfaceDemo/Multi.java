package interfaceDemo;

// Multiple inheritance achieved with interface
interface Continent{
	void continentName();
}
interface Country{
	void countryName();
}
interface City{
	void cityName();
}

public class Multi implements Continent, Country, City {

	@Override
	public void cityName() {
		System.out.println("Copenhagen");
	}

	@Override
	public void countryName() {
		System.out.println("Denmark");
	}

	@Override
	public void continentName() {
		System.out.println("Europe");
	}
	
	public static void main(String[] args) {
		Multi m = new Multi();
		m.cityName();
		m.countryName();
		m.continentName();
		
	}

}
