package collections;

import java.util.LinkedList;

public class LinkedListDemo {

	public static void main(String[] args) {
		LinkedList<String> sports= new LinkedList<>();
		
		sports.add("Tennis");    // to add element
		sports.add("Volleyball");
		System.out.println(sports);
		
		sports.add(0, "kho-kho");   // to add element at particular index
		System.out.println(sports);
		
		sports.addLast("Cricket");  // add element at LAST of the array
		System.out.println(sports);
		
		String s = sports.get(3);   // get the element on the list
		System.out.println(s);
		
		sports.addFirst("somegame"); // add element at FIRST of the array
		System.out.println(sports);
	}

}
