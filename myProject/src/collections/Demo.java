package collections;

import java.util.ArrayList;
import java.util.List;

public class Demo {

	public static void main(String[] args) {
		
			List<String> list = new ArrayList<>();
			
			list.add("New York");
			list.add("Rome");
			list.add("Paris");
			list.add("Copenhagen");
			list.add("New Delhi");
			list.add("Amsterdam");
			list.add("12345");
			list.add("Kuala Lumpur");
			list.add(6, "Stockholm"); // to add element at particular index number
			
			list.set(5, "Mumbai"); // to replace element at particular index number
			
			System.out.println(list);
			System.out.println("Capital City of Denmark : " +list.get(3));
			
			list.remove(0); // to remove element
			System.out.println(list);
			
			list.add(2, "Canberra"); // to add at particular index number
			System.out.println(list);
			
			int tsize = list.size(); // to find the size of list
			System.out.println(tsize);
			
			boolean c = list.contains("12345"); // to find whether it present or not
			System.out.println(c);
	}

}
