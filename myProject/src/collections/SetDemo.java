package collections;

import java.util.ArrayList;
import java.util.List;

public class SetDemo {

	public static void main(String[] args) {
		List<String> sports = new ArrayList<>();
		
		sports.add("Tennis");
		sports.add("Football");
		sports.add("Kabaddi");
		sports.add("Badminton");
		System.out.println(sports);
		
		sports.add("Badminton"); // duplicate value
		sports.add("Tennis");
		System.out.println(sports);
		
		int s = sports.size(); // to find size
		System.out.println(s);
	}

}
