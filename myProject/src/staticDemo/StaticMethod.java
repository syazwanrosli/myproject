package staticDemo;

public class StaticMethod {
	
	void play() { // regular method
		System.out.println("I play Tennis...");
	}
	
	// static method
	static void sleep () {
		System.out.println("I slepp at 9..."); // no need to call object to call static method
	}

	public static void main(String[] args) {
			sleep(); // static method
			StaticMethod sm = new StaticMethod(); // create object for regular method
			sm.play(); 
	}

}
