package basics;

public class First { // class created
	
	public void mymethod() { // method creation
		System.out.println("My first method logic..");
		
	}
	
	 public void method2() {
		System.out.println("My second method logic..");
	}

	public static void main(String[] args) {
			System.out.println("This is my first JAVA program");
			
			// Object creation
			First abc = new First();
			
			abc.mymethod();
			abc.method2();
	}
	

}
