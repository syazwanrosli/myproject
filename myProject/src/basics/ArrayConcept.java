package basics;

public class ArrayConcept {

	public static void main(String[] args) {
		//array can store multiple values 
		
		int [] age = {20,25,19,12,45,90,78,5,23,25};
		System.out.println(age[2]);
		System.out.println(age.length);
		
		String [] namesofStudents = new String [10];
		namesofStudents [0] = "Ricky";
		namesofStudents [1] = "Lucky";
		namesofStudents [2] = "Sachin";
		
		System.out.println(namesofStudents [2]);
	}
}
