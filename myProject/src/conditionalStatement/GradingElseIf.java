package conditionalStatement;

import java.util.Scanner;

public class GradingElseIf {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
				
		System.out.println("Enter Your Science Marks :");
		int science = sc.nextInt();
		
		System.out.println("Enter Your Maths Marks :");
		int maths = sc.nextInt();
		
		System.out.println("Enter Your English Marks :");
		int english = sc.nextInt();
		
		int total = science+maths+english;
		System.out.println("Your Total Marks : " +total);
		
		float avg = total/3;
		System.out.println("Your Average Score is : " +avg);
		
		
		
		if(avg>=90) {
			System.out.println("Congrats! You got 'A'");
		}
		
		else if(avg>=70 && avg<90) {
			System.out.println("Congrats! You got 'B', keep working! ");
		}
		
		else if(avg>=50 && avg<70) {
			System.out.println("You got 'C', Please Work Hard! ");
		}
		
		else {
			System.out.println("You are FAILED!!");
		}
	}
}
