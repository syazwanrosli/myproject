package conditionalStatement;

public class WhileLoop {

	public static void main(String[] args) {
		int a = 1;
		while(a<=10) {
			System.out.println("Print value a : " +a);
			a++;
		}
	}

}
