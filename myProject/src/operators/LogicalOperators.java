package operators;

public class LogicalOperators {

	public static void main(String[] args) {
		// AND &&    OR ||   NOT !
		// && - both conditions should be true
		System.out.println((100>50)&&(20>10));
		
		// || OR - any conditions is true then its true
		System.out.println((10>5)||(10>12));
		
		
	}

}
