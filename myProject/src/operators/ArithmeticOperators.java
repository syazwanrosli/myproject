package operators;

public class ArithmeticOperators {
		// +  -  *  /  %
	
	public static void main(String[] args) {
//		int a = 10; // declared and assigned value to variable 'a'
//		int b =20;
		
	int a=10, b=20;
	
	System.out.println ("Additional Operator : " +(a+b));
	System.out.println ("Subtraction Operator : " +(b-a));
	System.out.println ("Multiplication Operator : " +(a*b));
	System.out.println ("Division Operator : " +(a/b));
	System.out.println ("Modulus Operator : " +(a%b));

	}

}
