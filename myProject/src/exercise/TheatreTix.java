package exercise;

import java.util.Scanner;

public class TheatreTix {

	public static void main(String[] args) {
			
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter no of tickets : ");
		int tix = sc.nextInt();
		sc.nextLine();
		System.out.println("Do you need any refreshments? : ");
		String r = sc.nextLine();
		System.out.println("Using Coupon? : ");
		String c = sc.nextLine();
		System.out.println("Ticket type : ");
		String t = sc.nextLine();
		
		if(tix<5 && tix>40) {
			System.out.println("Minimum of 5 and Maximum of 40 only!");
		}else if(!t.equals("q") && !t.equals("k")) {
			System.out.println("Invalid Input");
		}
		
		else {
			if(tix<=20) {
				if(r.equals("y")) {
					if(c.equals("y")) {
						if(t.equals("k")) {
							double total = (tix*75*0.98)+(tix*50);
							System.out.println("Total Cost : " +total);

						}else {
							double total = (tix*150*0.98)+(tix*50);
							System.out.println("Total Cost : " +total);

						}
					}else {
						if(t.equals("q")) {
							double total = (tix*75*0.98)+(tix*50);
							System.out.println("Total Cost : " +total);

						}else {
							double total = (tix*150*0.98)+(tix*50);
							System.out.println("Total Cost : " +total);

						}
					}
				}else {
					if(c.equals("n")) {
						if(t.equals("k")) {
							double total = (tix*75)+(tix*50);
							System.out.println("Total Cost : " +total);

						}else {
							double total = (tix*150)+(tix*50);
							System.out.println("Total Cost : " +total);

						}
					}else {
						if(t.equals("q")) {
							double total = (tix*75)+(tix*50);
							System.out.println("Total Cost : " +total);

						}else {
							double total = (tix*150)+(tix*50);
							System.out.println("Total Cost : " +total);

						}
					}
				} // tutup
				
			}else { //ticket>20
				if(r.equals("y")) {
					if(c.equals("y")) {
						if(t.equals("k")) {
							double total = (tix*75*0.98*0.90)+(tix*50);
							System.out.println("Total Cost : " +total);

						}else {
							double total = (tix*150*0.98*0.90)+(tix*50);
							System.out.println("Total Cost : " +total);

						}
					}else {
						if(t.equals("q")) {
							double total = (tix*75*0.980*0.90)+(tix*50);
							System.out.println("Total Cost : " +total);

						}else {
							double total = (tix*150*0.98*0.90)+(tix*50);
							System.out.println("Total Cost : " +total);

						}
					}
				}else {
					if(c.equals("y")) {
						if(t.equals("k")) {
							double total = (tix*75*0.98)+(tix*50);
							System.out.println("Total Cost : " +total);

						}else {
							double total = (tix*150*0.98)+(tix*50);
							System.out.println("Total Cost : " +total);

						}
					}else {
						if(t.equals("q")) {
							double total = (tix*75*0.90)+(tix*50);
							System.out.println("Total Cost : " +total);

						}else {
							double total = (tix*150*0.90)+(tix*50);
							System.out.println("Total Cost : " +total);

						}
					}
				} // tutup
				} // tutup
			
			}
  }
}
