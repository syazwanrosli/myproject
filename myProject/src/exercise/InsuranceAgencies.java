package exercise;

import java.util.Scanner;

public class InsuranceAgencies {
	
	public void calculateAverage(int[] ages) {
		
		double m = 0;
		for(int j=0;j<ages.length;j++) {
			 m = m+ages[j];
		}
		double l = m/ages.length;
		System.out.println("The Average Age is "+l);
	}

	public static void main(String[] args) {
			
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter Number of Employees : ");
		int emp = sc.nextInt();
		int arr[] = new int[emp];
		
		if(emp<2) {
			System.out.println("Enter Valid No of Employees!");
		}else {
			System.out.println("Enter age of " +emp+ " employees : ");
			for(int i=0; i<arr.length; i++) {
				arr[i]=sc.nextInt();
				if(arr[i]<28 || arr[i]>40) {
					System.out.println("Enter valid age limit of employees between 28-40y/o");
					System.exit(1);
				}
			}
		}
		InsuranceAgencies k = new InsuranceAgencies();
		k.calculateAverage(arr);
	}

}
