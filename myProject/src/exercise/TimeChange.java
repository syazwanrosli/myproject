package exercise;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class TimeChange {

	public static void main(String[] args) throws ParseException {
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter 12 hour format time");
			String s = sc.nextLine();
			
			DateFormat inFormat = new SimpleDateFormat("hh:mm:ss");
			DateFormat outFormat = new SimpleDateFormat("hh:mm:ss");
			
			Date date=null;
			date=inFormat.parse(s);
			
			if (date != null) {
				String myDate = outFormat.format(date);
				System.out.println(myDate);
			}
			sc.close();
	}

}
