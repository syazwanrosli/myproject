package exercise;

import java.util.Scanner;

public class Exercise1 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter your first number : ");
		double no1 = sc.nextFloat(); 
		
		System.out.print("Enter your second number : ");
		double no2 = sc.nextFloat();
		
		double total = no1 * no2;
		
		System.out.println("The Total of " + no1 + " Multiply by " + no2 + " : " + total);
		
		
	}

}
